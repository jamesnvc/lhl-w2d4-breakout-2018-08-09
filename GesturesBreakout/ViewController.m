//
//  ViewController.m
//  GesturesBreakout
//
//  Created by James Cash on 09-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "StuffDoer.h"

@interface ViewController ()
@property (nonatomic,strong) StuffDoer *thingy;
@property (nonatomic,assign) NSInteger movedTimes;
@property (strong, nonatomic) IBOutlet UIView *greenView;
@property (nonatomic,assign) CGPoint initialLocation;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.thingy = [[StuffDoer alloc] init];

    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(sawPan:)];
//                                   initWithTarget:self.thingy
//                                   action:@selector(handlePanGesture:)];
    // ^ is using target-action pattern
    // the idea is that we have the "action" which is the selector (i.e. the name of the method to call) and the "target" which is the specific object the method should be invoked on
    // so [foo setTarget:bar action:@selector(baz)]
    // means "hey foo, when the thing happens, call `[bar baz]`"

    // then we need to give the gesture recognizer something that it can recoginize events in
//    [self.view addGestureRecognizer:pan];
    // we don't need to do this in this particular case, but in general, you need to make sure that user-interaction is enabled on the view or it won't get touch events, and so won't be able to give them to the recognizer
    self.view.userInteractionEnabled = YES;

    UIScreenEdgePanGestureRecognizer *edgeRec = [[UIScreenEdgePanGestureRecognizer alloc]init];
    edgeRec.edges = UIRectEdgeTop | UIRectEdgeRight;
}

// IBAction is the same as void, just gives the little bubble in the sidebar to drag to Interface Builder
- (void)sawPan:(UIPanGestureRecognizer*)sender
{
//    NSLog(@"Entering handler function");
    // important to keep in mind that this method will be called many many times as the gesture moves
    // so it'll becalled once (state Began) when the touch starts
    // then it'll be called every time the gesture moves (changed)
    // then once more when it ends
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
//            NSLog(@"Gesture started");
            self.movedTimes = 0;
            break;
        case UIGestureRecognizerStateChanged:
//            NSLog(@"Changed");
            self.movedTimes += 1;
            break;
        case UIGestureRecognizerStateEnded:
            NSLog(@"Ended: moved %ld times", self.movedTimes);
            break;
        default:
            break;
    }
//    NSLog(@"Finished handler function");
}

- (IBAction)longPressOnSquare:(UILongPressGestureRecognizer *)sender {

    if (CGRectContainsPoint(self.greenView.frame, sender.view.center)) {
//    if (CGRectIntersectsRect(sender.view.frame, self.greenView.frame)) {
//    if (CGRectContainsRect(self.greenView.frame, sender.view.frame)) {
        self.greenView.backgroundColor = UIColor.orangeColor;
    } else {
        self.greenView.backgroundColor = UIColor.greenColor;
    }

    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            self.initialLocation = sender.view.center;
            CGPoint touchLoc = [sender locationInView:self.view];
            sender.view.center = touchLoc;
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            UIView *parentView = sender.view.superview; // = self.view
            // we want to get the location of the touch in the coordinate system of the parent view, because we're going to use this to position the sender.view (i.e. our red view in the storyboard) and that is done in the coordinate system of the view's parent (which is self.view)
            CGPoint loc = [sender locationInView:parentView];
            sender.view.center = loc;
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            [UIView animateWithDuration:2 animations:^{
                sender.view.center = self.initialLocation;
            }];
        }
            break;
        default:
            NSLog(@"Something else");
            break;
    }
}
- (IBAction)swipeOnRed:(UISwipeGestureRecognizer *)sender {
    int x = arc4random_uniform(300);
    int y = arc4random_uniform(300);
    sender.view.center = CGPointMake(x, y);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
