//
//  StuffDoer.h
//  GesturesBreakout
//
//  Created by James Cash on 09-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StuffDoer : NSObject

- (void)handlePanGesture:(id)sender;

@end
